
## Configuración

 - Por terminal agregar aws configure , ingresar acces_key y secret_key correspondientes.
 - En el archivo github.tf modificar los valores del repositorio como token, owner y repository.
 - Ejecutar los comandos para construir la infraestructura:


```
$ terraform init 
$ terraform plan 
$ terraform apply

```
## Finalmente Se solicita ingresar el token de Github en consola para cargar las secrets en el repositorio correspondiente.

## Instalación de terraform docs con chocolatey por powershell de manera global


```
choco install terraform-docs

```

### Ejecutar por terminal en el proyecto

```
terraform-docs markdown . > Terraformdocs.md

```


### README Generado:



## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |
| <a name="provider_github"></a> [github](#provider\_github) | n/a |
| <a name="provider_tls"></a> [tls](#provider\_tls) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_vpc"></a> [vpc](#module\_vpc) | terraform-aws-modules/vpc/aws | 5.6.0 |

## Resources

| Name | Type |
|------|------|
| [aws_instance.COMPRAS_dev_instance](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance) | resource |
| [aws_key_pair.ec2-server-ssh-key](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/key_pair) | resource |
| [aws_security_group.instance_security_group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [github_actions_secret.host](https://registry.terraform.io/providers/integrations/github/latest/docs/resources/actions_secret) | resource |
| [github_actions_secret.ssh_private_key](https://registry.terraform.io/providers/integrations/github/latest/docs/resources/actions_secret) | resource |
| [tls_private_key.ssh_key](https://registry.terraform.io/providers/hashicorp/tls/latest/docs/resources/private_key) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_ami_id"></a> [ami\_id](#input\_ami\_id) | ID de la AMI | `string` | `"ami-0a3c3a20c09d6f377"` | no |
| <a name="input_associate_public_ip_address"></a> [associate\_public\_ip\_address](#input\_associate\_public\_ip\_address) | Asociar direcci├│n IP p├║blica | `bool` | `true` | no |
| <a name="input_enable_nat_gateway"></a> [enable\_nat\_gateway](#input\_enable\_nat\_gateway) | Habilitar el Nat Gateway | `bool` | `true` | no |
| <a name="input_enable_vpn_gateway"></a> [enable\_vpn\_gateway](#input\_enable\_vpn\_gateway) | Habilitar el Vpn Gateway | `bool` | `true` | no |
| <a name="input_github_owner"></a> [github\_owner](#input\_github\_owner) | Propietario del repositorio en GitHub | `string` | `"alkemyTech"` | no |
| <a name="input_github_repository"></a> [github\_repository](#input\_github\_repository) | Nombre del repositorio en GitHub | `string` | `"UMSA-DevOps-T4"` | no |
| <a name="input_github_token"></a> [github\_token](#input\_github\_token) | n/a | `any` | n/a | yes |
| <a name="input_host_secret_name"></a> [host\_secret\_name](#input\_host\_secret\_name) | Nombre del secreto para el host en GitHub | `string` | `"HOST_COMPRAS"` | no |
| <a name="input_instance_tags"></a> [instance\_tags](#input\_instance\_tags) | Etiquetas para la instancia EC2 | `map(string)` | <pre>{<br>  "Name": "ORG-COMPRAS-DEV"<br>}</pre> | no |
| <a name="input_instance_type"></a> [instance\_type](#input\_instance\_type) | Tipo de instancia | `string` | `"t2.micro"` | no |
| <a name="input_private_subnets"></a> [private\_subnets](#input\_private\_subnets) | Subredes privadas | `list(string)` | <pre>[<br>  "10.0.1.0/24",<br>  "10.0.2.0/24"<br>]</pre> | no |
| <a name="input_public_subnets"></a> [public\_subnets](#input\_public\_subnets) | Subredes p├║blicas | `list(string)` | <pre>[<br>  "10.0.101.0/24",<br>  "10.0.102.0/24"<br>]</pre> | no |
| <a name="input_ssh_private_key_secret_name"></a> [ssh\_private\_key\_secret\_name](#input\_ssh\_private\_key\_secret\_name) | Nombre del secreto para la clave SSH en GitHub | `string` | `"SSH_PRIVATE_KEY_COMPRAS"` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Etiquetas para la VPC | `map(string)` | <pre>{<br>  "Environment": "dev",<br>  "Terraform": "true"<br>}</pre> | no |
| <a name="input_vpc_azs"></a> [vpc\_azs](#input\_vpc\_azs) | Zonas de disponibilidad de la VPC | `list(string)` | <pre>[<br>  "us-east-1b",<br>  "us-east-1c"<br>]</pre> | no |
| <a name="input_vpc_cidr"></a> [vpc\_cidr](#input\_vpc\_cidr) | vpc cidr | `string` | `"10.0.0.0/16"` | no |
| <a name="input_vpc_name"></a> [vpc\_name](#input\_vpc\_name) | nombre de la vpc | `string` | `"vpc-terraform-entregable"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_public_ip_dev"></a> [public\_ip\_dev](#output\_public\_ip\_dev) | Salida para mostrar la IP p├║blica de la instancia EC2 despu├®s del despliegue |
| <a name="output_ssh_private_key"></a> [ssh\_private\_key](#output\_ssh\_private\_key) | n/a |
